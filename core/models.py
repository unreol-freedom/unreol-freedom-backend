from django.db import models

class TagModel(models.Model):
    title = models.CharField("Tag Title", max_length=197)

    def __str__(self):
        return self.title

class ProjectModel(models.Model):
    image_url = models.URLField("Image Url")
    project_url = models.URLField("Project Url")
    tags = models.ManyToManyField(TagModel)
    
    title = models.CharField("Title", max_length=3000)
    description = models.TextField("Description", max_length=5000)

    def __str__(self):
        return self.title
