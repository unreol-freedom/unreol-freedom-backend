from .models import ProjectModel, TagModel
from django.http import JsonResponse


def list_view(request):
    result = []

    projects = ProjectModel.objects.all()

    for project in projects:
        p = {
            "title": project.title,
            "description": project.description,
            "image_url": project.image_url,
            "project_url": project.project_url,
        }
        tags = []

        for tag in project.tags.all():
            tags.append(tag.title)

        p["tags"] = tags
        result.append(p)

    return JsonResponse({"projects": result})
