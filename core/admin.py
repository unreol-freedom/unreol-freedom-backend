from django.contrib import admin
from .models import ProjectModel, TagModel

admin.site.register(ProjectModel)
admin.site.register(TagModel)
